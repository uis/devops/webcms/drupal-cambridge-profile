api = 2
core = 7.x

defaults[projects][subdir] = "contrib"

; theme

projects[cambridge_theme][type] = "theme"
projects[cambridge_theme][version] = "1.25"
projects[cambridge_theme][download][type] = "file"
projects[cambridge_theme][download][url] = "https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-cambridge-theme/-/archive/7.x-1.25/drupal-cambridge-theme-7.x-1.25.tar.gz"
projects[cambridge_theme][subdir] = ""

; contrib

projects[ctools] = "1.21"
projects[features][version] = "2.15"
projects[features][subdir] = "contrib"

projects[imagecrop][version] = "1.1"
projects[imagecrop][type] = "module"
projects[imagecrop][download][type] = "file"
projects[imagecrop][download][url] = "https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/imagecrop/-/archive/1.1/imagecrop-1.1.tar.gz"
projects[jquery_update] = "4.1"
projects[libraries] = "2.5"
projects[link][version] = "1.11"
projects[link][patch][] = "https://www.drupal.org/files/issues/2022-08-02/link_7.x-1.x-undo-query-string-removal-3257043-10.patch"
projects[menu_block] = "2.9"
projects[menu_firstchild][patch][] = "http://www.drupal.org/files/issues/pathauto-token-2295059-1.patch"
projects[pathauto] = "1.3"
;projects[pathauto_persist] = "1.4"
projects[raven][type] = "module"
projects[raven][download][type] = "file"
projects[raven][download][url] = "https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/raven/-/archive/1.7/raven-1.7.tar.gz"
;projects[raven][download][url] = "https://github.com/misd-service-development/drupal-raven/archive/7.x-1.3.tar.gz"
;projects[raven][patch][] = "https://github.com/misd-service-development/drupal-raven/commit/cfff4cbf50a03ec3806da4a9ce11c29773fbc0eb.patch"
projects[token] = "1.9"
projects[views] = "3.29"

; features

projects[cambridge_carousel][type] = "module"
projects[cambridge_carousel][download][type] = "file"
projects[cambridge_carousel][download][url] = "https://github.com/misd-service-development/drupal-feature-carousel/archive/7.x-1.1.tar.gz"
projects[cambridge_carousel][subdir] = "features"

projects[cambridge_image_styles][type] = "module"
projects[cambridge_image_styles][download][type] = "file"
projects[cambridge_image_styles][download][url] = "https://github.com/misd-service-development/drupal-feature-image-styles/archive/7.x-1.1.tar.gz"
projects[cambridge_image_styles][subdir] = "features"

projects[cambridge_link][type] = "module"
projects[cambridge_link][download][type] = "file"
projects[cambridge_link][download][url] = "https://github.com/misd-service-development/drupal-feature-link/archive/7.x-1.0.tar.gz"
projects[cambridge_link][subdir] = "features"

projects[cambridge_teasers][type] = "module"
projects[cambridge_teasers][version] = "1.10"
projects[cambridge_teasers][download][type] = "file"
projects[cambridge_teasers][download][url] = "https://gitlab.developers.cam.ac.uk/uis/devops/webcms/drupal-7-features/cambridge_teasers/-/archive/7.x-1.10/cambridge_teasers-7.x-1.10.tar.gz"
projects[cambridge_teasers][subdir] = "features"

projects[simple_lookup][type] = "module"
projects[simple_lookup][download][type] = "file"
projects[simple_lookup][download][url] = "https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/simple_lookup/-/archive/1.3/simple_lookup-1.3.tar.gz"
projects[simple_lookup][subdir] = "custom"
